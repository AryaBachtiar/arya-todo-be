package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.AbsenDto;
import com.example.PresensiToDoList.dto.AbsenPulangDto;
import com.example.PresensiToDoList.model.Absen;

import java.util.List;

public interface AbsenService {

    List<Absen> getAllAbsen(Long usersId);

    public Absen addPresensiPulang(AbsenPulangDto absenPulangDto, boolean type);

    public Absen addPresensiMasuk(AbsenDto absenDto, boolean type);

//    Absen editAbsen(Long id, Long passagerTypeId, Absen absen);

    Absen getAbsen(Long id);

    void deleteAbsenById(Long id);
}
