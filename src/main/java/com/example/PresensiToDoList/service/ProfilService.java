package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.ProfilDto;
import com.example.PresensiToDoList.model.Profil;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProfilService {

    List<Profil> getAllProfil(Long usersId);

    Profil addProfil(ProfilDto profil , MultipartFile multipartFile);

    Profil editProfil(Long id, Profil profil, MultipartFile multipartFile);

    Profil getProfil(Long userId);

    void deleteProfilById(Long id);

}
