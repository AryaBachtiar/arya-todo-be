package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.LoginDto;
import com.example.PresensiToDoList.model.Users;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface UsersService {
    Map<String, Object> login(LoginDto loginDto);
    Object getAllUsers();
    Users addUsers(Users users);

    Users getUsers(Long id);
    Users editUsers(Long id,Users users);

    void deleteUsersById(Long id);
}

