package com.example.PresensiToDoList.service;

import com.example.PresensiToDoList.dto.TodoListDto;
import com.example.PresensiToDoList.model.TodoList;

import java.util.List;

public interface TodoListService {
    List<TodoList> getAllTodoList(Long userId);

    TodoList addtodoList(TodoListDto todoListDto);

    TodoList getTodoList(Long id);

    TodoList editTodoList(Long id, TodoList todoList);


    void deleteTodoListById(Long id);
}
