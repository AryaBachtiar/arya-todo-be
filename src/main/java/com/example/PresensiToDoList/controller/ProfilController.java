package com.example.PresensiToDoList.controller;

import com.example.PresensiToDoList.dto.ProfilDto;
import com.example.PresensiToDoList.dto.ProfilDto;
import com.example.PresensiToDoList.model.Profil;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponseHelper;
import com.example.PresensiToDoList.service.ProfilService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/profil")
public class ProfilController {

    @Autowired
    private ModelMapper modalmepper;

    @Autowired
    private ProfilService profilService;

    @GetMapping("/all")
    public CommonResponse<List<Profil>> getAllProfil(Long userId){
        return ResponseHelper.ok(profilService.getAllProfil(userId)) ;
    }

    @GetMapping("/{id}")
    public CommonResponse<Profil>  getProfil(@PathVariable("id") Long userId) {
        return ResponseHelper.ok(profilService.getProfil(userId)) ;
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public CommonResponse<Profil> addProfil( ProfilDto profilDto, @RequestPart("foto")MultipartFile multipartFile) {
        return ResponseHelper.ok( profilService.addProfil(profilDto, multipartFile)) ;
    }

    @PutMapping("/{id}")
    public CommonResponse<Profil> editProfilaById(@PathVariable("id") Long id , @RequestBody ProfilDto profilDto, MultipartFile multipartFile) {
        return ResponseHelper.ok(profilService.editProfil(id,modalmepper.map(profilDto,Profil.class),multipartFile)) ;
    }

    @DeleteMapping("/{id}")
    public void deleteProfilById(@PathVariable("id") Long id) { profilService.deleteProfilById(id);}
}
