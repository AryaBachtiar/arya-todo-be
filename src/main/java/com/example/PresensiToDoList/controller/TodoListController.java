package com.example.PresensiToDoList.controller;

import com.example.PresensiToDoList.dto.TodoListDto;
import com.example.PresensiToDoList.model.TodoList;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponseHelper;
import com.example.PresensiToDoList.service.TodoListService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todolist")
public class TodoListController {

    @Autowired
    private TodoListService todolistService;

    @Autowired
    private ModelMapper modalmepper;

    @GetMapping("/all")
    public CommonResponse<List<TodoList>> getAllTodoList(Long userId){
        return ResponseHelper.ok(todolistService.getAllTodoList(userId)) ;
    }

    @GetMapping("/{id}")
    public CommonResponse  getTodoList(@PathVariable("id") Long id) {
        return ResponseHelper.ok(todolistService.getTodoList(id)) ;
    }

    @PostMapping
    public CommonResponse<TodoList> addTodoList(@RequestBody TodoListDto todoListDto) {
        return ResponseHelper.ok( todolistService.addtodoList(todoListDto)) ;
    }

    @PutMapping("/{id}")
    public CommonResponse<TodoList> editTodoListaById(@PathVariable("id") Long id , @RequestBody TodoListDto todolistDto) {
        return ResponseHelper.ok(todolistService.editTodoList(id,modalmepper.map(todolistDto,TodoList.class))) ;
    }

    @DeleteMapping("/{id}")
    public void deleteTodoListById(@PathVariable("id") Long id) { todolistService.deleteTodoListById(id);}
}
