//package com.example.PresensiToDoList.controller;
//
//import com.example.PresensiToDoList.dto.AbsenDto;
//import com.example.PresensiToDoList.dto.AbsenMasukDto;
//import com.example.PresensiToDoList.model.Absen;
//import com.example.PresensiToDoList.model.AbsenMasuk;
//import com.example.PresensiToDoList.response.CommonResponse;
//import com.example.PresensiToDoList.response.ResponseHelper;
//import com.example.PresensiToDoList.service.AbsenMasukService;
//import com.example.PresensiToDoList.service.AbsenService;
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequestMapping("/absenmasuk")
//public class AbsenMasukController {
//
//    @Autowired
//    private AbsenMasukService absenMasukService;
//
//    @Autowired
//    private ModelMapper modalmepper;
//
//    @GetMapping("/all")
//    public Object getAllAbsenMasuk(Long userId){
//        return ResponseHelper.ok(absenMasukService.getAllAbsenMasuk(userId)) ;
//    }
//
//    @GetMapping("/{id}")
//    public CommonResponse  getAbsenMasuk(@PathVariable("id") Long id) {
//        return ResponseHelper.ok(absenMasukService.getAbsenMasuk(id)) ;
//    }
//
//    @PostMapping
//    public CommonResponse<AbsenMasuk> addAbsenMasuk(AbsenMasukDto absenMasukDto) {
//        return ResponseHelper.ok( absenMasukService.addabsenmasuk(absenMasukDto));
//    }
//
////    @PutMapping("/{id}")
////    public CommonResponse<Absen> editAbsenaById(@PathVariable("id") Long id, @RequestParam("passagerTypeId") Long passagerTypeId, @RequestBody AbsenDto absenDto) {
////        return ResponseHelper.ok(absenService.editAbsen(id,passagerTypeId,modalmepper.map(AbsenDto,Absen.class))) ;
////    }
//
//    @DeleteMapping("/{id}")
//    public void deleteAbsenMasukById(@PathVariable("id") Long id) { absenMasukService.deleteAbsenMasukById(id);}
//}
