package com.example.PresensiToDoList.controller;

import com.example.PresensiToDoList.dto.LoginDto;
import com.example.PresensiToDoList.dto.ProfilDto;
import com.example.PresensiToDoList.dto.UserDto;
import com.example.PresensiToDoList.model.Users;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponseHelper;
import com.example.PresensiToDoList.service.UsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/user")
public class UsersController {

    @Autowired
    private UsersService usersService;


    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String,Object>>login(@RequestBody LoginDto loginDto){
        return ResponseHelper.ok(usersService.login(loginDto));
    }
    @PostMapping(path = "/sign-up")
    public CommonResponse<Users>addUsers(UserDto userDto){
        return ResponseHelper.ok(usersService.addUsers(modelMapper.map(userDto,Users.class)));
    }
    @GetMapping("/all")
    public  Object getAllUsers(){
        return ResponseHelper.ok(usersService.getAllUsers());

    }
    @GetMapping("/{id}")
    public CommonResponse<Users> getUsers(@PathVariable("id")Long id){
        return ResponseHelper.ok(usersService.getUsers(id));
    }

    //        @PostMapping
//    public CommonResponse<Users> addUsers(@RequestBody Users users) {
//        return ResponseHelper.ok( usersService.addUsers(users)) ;
//    }
    @PutMapping(path = "/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public CommonResponse<Users> editUsersById(@PathVariable("id") Long id, ProfilDto profilDto) {
        return ResponseHelper.ok( usersService.editUsers( id,modelMapper.map(profilDto, Users.class) ));
    }
    @DeleteMapping("/{id}")
    public void deleteUsersById(@PathVariable("id") Long id) { usersService.deleteUsersById(id);}
}


