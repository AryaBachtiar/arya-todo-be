package com.example.PresensiToDoList.controller;

import com.example.PresensiToDoList.dto.AbsenDto;
import com.example.PresensiToDoList.dto.AbsenPulangDto;
import com.example.PresensiToDoList.model.Absen;
import com.example.PresensiToDoList.response.CommonResponse;
import com.example.PresensiToDoList.response.ResponseHelper;
import com.example.PresensiToDoList.service.AbsenService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/absen")
public class AbsenController {

    @Autowired
    private AbsenService absenService;

    @Autowired
    private ModelMapper modalmepper;

    @GetMapping("/all")
    public Object getAllAbsen(Long userId){
        return ResponseHelper.ok(absenService.getAllAbsen(userId)) ;
    }

    @GetMapping("/{id}")
    public CommonResponse  getAbsen(@PathVariable("id") Long id) {
        return ResponseHelper.ok(absenService.getAbsen(id)) ;
    }

    @PostMapping("/ABSEN_MASUK")
    public CommonResponse<Absen> addAbsenMasuk(AbsenDto absenDto) {
        return ResponseHelper.ok( absenService.addPresensiMasuk(absenDto,true));
    }

    @PostMapping("/ABSEN_PULANG")
    public CommonResponse<Absen> addAbsenPulang(AbsenPulangDto absenPulangDto) {
        return ResponseHelper.ok( absenService.addPresensiPulang(absenPulangDto,false));
    }

//    @PutMapping("/{id}")
//    public CommonResponse<Absen> editAbsenaById(@PathVariable("id") Long id, @RequestParam("passagerTypeId") Long passagerTypeId, @RequestBody AbsenDto absenDto) {
//        return ResponseHelper.ok(absenService.editAbsen(id,passagerTypeId,modalmepper.map(AbsenDto,Absen.class))) ;
//    }

    @DeleteMapping("/{id}")
    public void deleteAbsenById(@PathVariable("id") Long id) { absenService.deleteAbsenById(id);}
}
