package com.example.PresensiToDoList.dto;

public class TodoListDto {

    private String tugas;



    private Long userId;

    public String getTugas() {
        return tugas;
    }

    public void setTugas(String tugas) {
        this.tugas = tugas;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
