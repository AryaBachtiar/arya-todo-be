package com.example.PresensiToDoList.dto;

import com.example.PresensiToDoList.enumted.PresensiEnum;

public class AbsenPulangDto {
    private Long usersId;

    private PresensiEnum presensi;

    public PresensiEnum getPresensi() {
        return presensi;
    }

    public void setPresensi(PresensiEnum presensi) {
        this.presensi = presensi;
    }

    public Long getUsersId() {
        return usersId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }
}
