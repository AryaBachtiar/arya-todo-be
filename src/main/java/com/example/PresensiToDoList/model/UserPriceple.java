package com.example.PresensiToDoList.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class UserPriceple implements UserDetails {

    private String email;

    private String password;

    public Collection<? extends GrantedAuthority> authority;


    public UserPriceple(String email, String password, Collection<? extends GrantedAuthority> authority){
        this.email = email;
        this.password =password;
        this.authority =authority;
    }


    public static UserPriceple build(Users users) {
        var role = Collections.singleton(new SimpleGrantedAuthority(users.getRole().name()));
        return new UserPriceple(
                users.getEmail(),
                users.getPassword(),
                role
        );
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
