package com.example.PresensiToDoList.model;

import com.example.PresensiToDoList.enumted.PresensiEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "absen")
public class Absen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @CreationTimestamp
    @Column(name = "tanggal")
    private Date tanggal;

    @JsonFormat(pattern = " HH:mm:ss")
    @CreationTimestamp
    @Column(name = "masuk")
    private Date masuk;


    @ManyToOne
    @JoinColumn(name = "users_id")
    private Users usersId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "absen")

    private PresensiEnum presensi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getMasuk() {
        return masuk;
    }

    public void setMasuk(Date masuk) {
        this.masuk = masuk;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    public PresensiEnum getPresensi() {
        return presensi;
    }

    public void setPresensi(PresensiEnum presensi) {
        this.presensi = presensi;
    }
}
