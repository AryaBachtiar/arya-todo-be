package com.example.PresensiToDoList.model;

import javax.persistence.*;

@Entity
@Table(name = "profil")
public class Profil  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "deskripsi")
    private String deskripsi;

    @Column (name = "nama")
    private String nama;

    @Column (name = "nomor")
    private String nomor;

    @Lob
    @Column(name = "foto")
    private String foto;

    @ManyToOne
    @JoinColumn(name = "users_id")
    private Users usersId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }
}
