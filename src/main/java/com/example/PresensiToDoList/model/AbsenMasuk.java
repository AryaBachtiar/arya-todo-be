//package com.example.PresensiToDoList.model;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//import org.hibernate.annotations.CreationTimestamp;
//
//import javax.persistence.*;
//import java.util.Date;
//
//@Entity
//@Table(name = "absenmasuk")
//public class AbsenMasuk {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    @JsonFormat(pattern = "yyyy-MM-dd")
//    @CreationTimestamp
//    @Column(name = "tanggal")
//    private Date tanggalmasuk;
//
//    @JsonFormat(pattern = " HH:mm:ss")
//    @CreationTimestamp
//    @Column(name = "absenmasuk")
//    private Date absenmasuk;
//
//    @ManyToOne
//    @JoinColumn(name = "users_id")
//    private Users usersId;
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public Date getTanggalmasuk() {
//        return tanggalmasuk;
//    }
//
//    public void setTanggalmasuk(Date tanggalmasuk) {
//        this.tanggalmasuk = tanggalmasuk;
//    }
//
//    public Date getAbsenmasuk() {
//        return absenmasuk;
//    }
//
//    public void setAbsenmasuk(Date absenmasuk) {
//        this.absenmasuk = absenmasuk;
//    }
//
//    public Users getUsersId() {
//        return usersId;
//    }
//
//    public void setUsersId(Users usersId) {
//        this.usersId = usersId;
//    }
//}
