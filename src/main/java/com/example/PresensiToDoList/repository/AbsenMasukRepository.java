//package com.example.PresensiToDoList.repository;
//
//import com.example.PresensiToDoList.model.Absen;
//import com.example.PresensiToDoList.model.AbsenMasuk;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface AbsenMasukRepository extends JpaRepository<AbsenMasuk, Long> {
//    @Query(value = "SELECT * FROM absenmasuk WHERE users_id = ?1", nativeQuery = true)
//    List<AbsenMasuk> findAllUser(Long userId);
//
//
//}
