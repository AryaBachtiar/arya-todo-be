package com.example.PresensiToDoList.repository;

import com.example.PresensiToDoList.model.Absen;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AbsenRepository extends JpaRepository<Absen , Long> {
    @Query(value = "SELECT * FROM absen WHERE users_id LIKE CONCAT('%', ?1, '%')", nativeQuery = true)
    List<Absen> findAllUser(Long userId);


}
