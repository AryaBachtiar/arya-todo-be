package com.example.PresensiToDoList.repository;


import com.example.PresensiToDoList.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TemporaryTokenRepository extends JpaRepository<TemporaryToken,Long> {
Optional<TemporaryToken> findByToken(String token);
Optional<TemporaryToken> findByUserId(Long userId);
}
