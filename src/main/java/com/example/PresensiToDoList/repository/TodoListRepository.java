package com.example.PresensiToDoList.repository;

import com.example.PresensiToDoList.model.TodoList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TodoListRepository extends JpaRepository<TodoList, Long> {

    @Query(value = "SELECT * FROM todolist WHERE users_id = ?1", nativeQuery = true)
    List<TodoList> findAllUser(Long usersId);
}
