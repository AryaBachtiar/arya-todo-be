//package com.example.PresensiToDoList.serviceImpl;
//
//import com.example.PresensiToDoList.dto.AbsenMasukDto;
//import com.example.PresensiToDoList.exception.NotFoundException;
//import com.example.PresensiToDoList.model.AbsenMasuk;
//import com.example.PresensiToDoList.repository.AbsenMasukRepository;
//import com.example.PresensiToDoList.repository.UsersRepository;
//import com.example.PresensiToDoList.service.AbsenMasukService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.Date;
//import java.util.List;
//
//@Service
//public class AbsenMasukServiceImpl implements AbsenMasukService {
//
//    private static final Integer hour = 3600 * 1000;
//    @Autowired
//    private AbsenMasukRepository absenMasukRepository;
//
//    @Autowired
//    private UsersRepository usersRepository;
//
//    @Override
//    public List<AbsenMasuk> getAllAbsenMasuk(Long usersId) {
//        return absenMasukRepository.findAllUser(usersId);
//    }
//
//    @Override
//    public AbsenMasuk addabsenmasuk(AbsenMasukDto absenMasukDto) {
//        AbsenMasuk absenmasuk1 = new AbsenMasuk();
//        absenmasuk1.setTanggalmasuk(new Date(new Date().getTime()));
//        absenmasuk1.setAbsenmasuk(new Date(new Date().getTime() + 7 * hour));
//        absenmasuk1.setUsersId(usersRepository.findById(absenMasukDto.getUsersId()).orElseThrow(() -> new NotFoundException("Not Found")));
//        return absenMasukRepository.save(absenmasuk1);
//    }
//
//    @Override
//    public AbsenMasuk getAbsenMasuk(Long id) {
//        var absen = absenMasukRepository.findById(id).get();
//        return absenMasukRepository.save(absen);
//    }
//
//
//
////    @Override
////    public AbsenMasuk editAbsenMasuk(Long id, Long passagerTypeId, AbsenMasuk absenMasuk) {
////AbsenMasuk update = absenMasukRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Tidak ditemukan"));
////    passagerTypeId passagerType1 = passagerTypeId(passagerTypeId).orElseThrow(() -> new NotFoundException("Passager tidak ada"));
////        update.setTanggalmasuk(absenMasuk.getTanggalmasuk());
////        update.setAbsenmasuk(absenMasuk.getAbsenmasuk());
////
////        return absenMasukRepository.save(update);
////    }
//
//    @Override
//    public void deleteAbsenMasukById(Long id) {
//        absenMasukRepository.deleteById(id);
//    }
//
////    @Override
////    public void deleteAbsenMasukById(Long id) {
////        absenMasukRepository.deleteById(id);
////        Map<String, Object> obj = new HashMap<>();
////        obj.put("DELETE" , true);
////        return obj;
////    }
//}
