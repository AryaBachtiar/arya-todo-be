package com.example.PresensiToDoList.serviceImpl;


import com.example.PresensiToDoList.dto.LoginDto;
import com.example.PresensiToDoList.enumted.Role;
import com.example.PresensiToDoList.exception.InternalErrorException;
import com.example.PresensiToDoList.jwt.JwtProvider;
import com.example.PresensiToDoList.model.Users;
import com.example.PresensiToDoList.repository.UsersRepository;
import com.example.PresensiToDoList.service.UsersService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UsersServiceImpl implements UsersService {
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    AuthenticationManager authenticationManager;

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/project-akhir-react.appspot.com/o/%s?alt=media";

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));

        } catch (BadCredentialsException e) {
            throw new InternalErrorException("email or password not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("error upload file ");
        }
    }

    private String getExtenions(String fileName) {
        return fileName.split("\\.")[0];
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("project-akhir-react.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        Users users = usersRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", users);
        return response;
    }


    @Override
    public Object getAllUsers() {
        try {
            return usersRepository.findAll();
        } catch (Exception e) {
            System.out.println("kamu nanya eror nya dimana");
            return "Kesalahan memunculkan data";
        }
    }

    @Override
    public Users addUsers(Users users) {
        String email = users.getEmail();

        users.setPassword(passwordEncoder.encode(users.getPassword()));


        users.setRole(Role.USER);
        var validasi = usersRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf email Sudah di gunakn");
        }
        return usersRepository.save(users);
    }

    @Override
    public Users getUsers(Long id) {
        var users = usersRepository.findById(id).get();
        return usersRepository.save(users);
    }

    @Override
    public Users editUsers(Long id, Users users) {
        Users users1 = usersRepository.findById(id).get();

        return usersRepository.save(users1);
    }

    private String convertToBase64Url(MultipartFile file) {
        String url = "";
        try {
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String result = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return url;
        }
    }

    @Override
    public void deleteUsersById(Long id) {
        usersRepository.deleteById(id);
    }
}

