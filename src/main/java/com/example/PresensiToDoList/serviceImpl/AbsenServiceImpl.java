package com.example.PresensiToDoList.serviceImpl;

import com.example.PresensiToDoList.dto.AbsenDto;
import com.example.PresensiToDoList.dto.AbsenPulangDto;
import com.example.PresensiToDoList.enumted.PresensiEnum;
import com.example.PresensiToDoList.exception.NotFoundException;
import com.example.PresensiToDoList.model.Absen;
import com.example.PresensiToDoList.repository.AbsenRepository;
import com.example.PresensiToDoList.repository.UsersRepository;
import com.example.PresensiToDoList.service.AbsenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class AbsenServiceImpl implements AbsenService {

    private static final Integer hour = 3600 * 1000;
    @Autowired
    private AbsenRepository absenRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public List<Absen> getAllAbsen(Long usersId) {
        return absenRepository.findAllUser(usersId);
    }

    @Override
    public Absen addPresensiMasuk(AbsenDto absenDto, boolean type) {
        Absen presensi1 = new Absen();
        presensi1.setMasuk(new Date(new Date().getTime() + 7 * hour));
        if (type == true) {
            presensi1.setPresensi(PresensiEnum.MASUK);

        }else {
            presensi1.setPresensi(PresensiEnum.PULANG);
        }
        presensi1.setUsersId(usersRepository.findById(absenDto.getUsersId()).orElseThrow(() -> new NotFoundException("Not Found")));
        return absenRepository.save(presensi1);
    }

    @Override
    public Absen addPresensiPulang(AbsenPulangDto absenPulangDto, boolean type) {
        Absen presensi1 = new Absen();
        presensi1.setMasuk(new Date(new Date().getTime() + 7 * hour));
        presensi1.setUsersId(usersRepository.findById(absenPulangDto.getUsersId()).orElseThrow(() -> new NotFoundException("Not Found")));
        presensi1.setPresensi(PresensiEnum.PULANG);
        return absenRepository.save(presensi1);
    }

    @Override
    public Absen getAbsen(Long id) {
        var absen = absenRepository.findById(id).get();
        return absenRepository.save(absen);
    }

//    @Override
//    public Absen editAbsen(Long id, Long passagerTypeId, Absen absen) {
//        return null;
//    }

    @Override
    public void deleteAbsenById(Long id) {
absenRepository.deleteById(id);
    }
}
