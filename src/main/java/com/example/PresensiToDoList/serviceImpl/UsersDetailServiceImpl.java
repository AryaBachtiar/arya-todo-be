package com.example.PresensiToDoList.serviceImpl;



import com.example.PresensiToDoList.model.UserPriceple;
import com.example.PresensiToDoList.model.Users;
import com.example.PresensiToDoList.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UsersDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username)throws UsernameNotFoundException{
        Users user = usersRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("Username Not Found"));
        return UserPriceple.build(user);
    }
}
