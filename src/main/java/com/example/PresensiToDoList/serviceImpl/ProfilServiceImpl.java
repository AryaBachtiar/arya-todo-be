package com.example.PresensiToDoList.serviceImpl;

import com.example.PresensiToDoList.dto.ProfilDto;
import com.example.PresensiToDoList.exception.InternalErrorException;
import com.example.PresensiToDoList.exception.NotFoundException;
import com.example.PresensiToDoList.model.Profil;
import com.example.PresensiToDoList.model.Profil;
import com.example.PresensiToDoList.model.Profil;
import com.example.PresensiToDoList.repository.ProfilRepository;
import com.example.PresensiToDoList.service.ProfilService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

@Service
public class ProfilServiceImpl implements ProfilService {

    @Autowired
    ProfilRepository profilRepository;

    @Autowired
    ProfilRepository usersRepository;


    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/project-akhir-react.appspot.com/o/%s?alt=media";

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("error upload file ");
        }
    }

    private String getExtenions(String fileName) {
        return fileName.split("\\.")[0];
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("project-akhir-react.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String convertToBase64Url(MultipartFile file) {
        String url = "";
        try {
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String result = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return url;
        }
    }

    @Override
    public List<Profil> getAllProfil(Long usersId) {
        return profilRepository.findAllProfil(usersId);
    }

    @Override
    public Profil addProfil(ProfilDto profilDto,MultipartFile multipartFile) {
        String foto = convertToBase64Url(multipartFile);
        Profil profil1 = new Profil();
        profil1.setFoto(foto);
        profil1.setDeskripsi(profilDto.getDeskripsi());
        profil1.setNomor(profilDto.getNomor());
        profil1.setUsersId(usersRepository.findById(profilDto.getUsersId()).orElseThrow(() -> new NotFoundException("Not Fount")).getUsersId());
        return profilRepository.save(profil1);
    }

    @Override
    public Profil editProfil(Long id, Profil profil, MultipartFile multipartFile) {
        Profil update = usersRepository.findById(id).get();
        String foto = convertToBase64Url(multipartFile);
        update.setNama(profil.getNama());
        update.setFoto(foto);
        return profilRepository.save(update);
    }

    @Override
    public Profil getProfil(Long userId) {
        return profilRepository.findById(userId).orElseThrow(() -> new NotFoundException("test"));
    }

    @Override
    public void deleteProfilById(Long id) {
profilRepository.deleteById(id);
    }
}
